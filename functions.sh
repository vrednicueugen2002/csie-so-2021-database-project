#!/bin/bash

numar=$1
fisier=$2

citire () {
	read -p "Nume si prenume: " nume
        read -p "Numar telefon: " mobil
	read -p "Varsta: " varsta
        read -p "Studii superioare (Da/Nu): " studii
        read -p "Experienta (numar ani): " experienta
        inregistrare="${nume} | ${mobil} | ${varsta} | ${studii} | ${experienta}"
	echo $inregistrare
}


case $numar in

	1)
		cat $fisier
		echo
		;;
	2)
		inregistrare=$(citire)
		echo $inregistrare >> $fisier
		echo  "Inregistrare adaugata cu succes!"
		;;
	3)
		#without sed command
		read -p "Introduceti numarul de telefon: " mobil
		inregistrare=$(citire)
		numarLinie=$(grep -n $mobil $fisier)
		numarLinie=${numarLinie::1}
		touch provizoriu.txt
		head -n $((numarLinie-1)) $fisier > provizoriu.txt
		echo $inregistrare >> provizoriu.txt
		liniiTotal=$(wc -l $fisier)
		liniiTotal=${liniiTotal::1}
		tail -n $((liniiTotal-numarLinie)) $fisier >> provizoriu.txt
		cat provizoriu.txt > $fisier
		rm provizoriu.txt
		echo "Modificare realizata cu succes!"
		;;
	4)
		read -p "Introduceti numarul de telefon: " mobil
		sed -i "/${mobil}/d" $fisier
		echo "Inregistrare stearsa cu succes!"
		;;
	5)
		read -p "Introduceti numarul de telefon: " mobil
		linie=$(grep $mobil $fisier)
		echo
		echo $linie
		;;
	6)
		read -p "Introduceti numarul vechi de telefon: " mobil
		read -p "Introduceti numarul nou de telefon: " nouMobil
		sed -i "s/$mobil/$nouMobil/" $fisier
		echo "Schimbare a numarului realizata cu succes!"
		;;
	*)
		echo "Alegeti un numar din cele de mai jos :)"
		;;
esac

sleep 2 &
wait
